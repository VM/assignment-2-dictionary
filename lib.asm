global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy

; new
global read_string
global err_string

section .text
 
%define SYS_EXIT 60
%define SYS_WRITE 1

%define STDIN 0
%define STDOUT 1
%define STDERR 2
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, SYS_EXIT          ; 'exit' syscall number
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    mov rax, -1
    .cycle:
        inc rax
        mov r10b, byte [rdi + rax]
        test r10b, r10b
        jne .cycle
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout/stderr
print_string:
    push rsi
    push r12
    mov r12, rdi             ; save address
    call string_length       ; rax now have string length
    mov     rdx, rax         ; string length
    mov     rax, SYS_WRITE   ; 'write' syscall number
    mov     rdi, STDOUT      ; stdout
    mov     rsi, r12         ; string address        
    syscall
    mov rdi, r12
    pop r12
    pop rsi
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov     rax, SYS_WRITE          ; 'write' syscall number
    mov     rdi, STDOUT          ; stdout
    mov     rsi, rsp        ; string address
    mov     rdx, 1          ; string length
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rax, rdi
    mov rdi, `\n`
    call print_char
    mov rdi, rax
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    mov r10, rdi
    test rdi, rdi
    jns .positive
    mov rdi, "-"
    push r10
    call print_char
    pop r10
    neg r10
    .positive:
    mov rdi, r10
; Выводит беззнаковое 8-байтовое число в десятичном формате 
print_uint:
    push r12
    xor rax, rax
    mov rax, rdi
    mov r12, -1         ; decimal numbers counter
    mov r11, 10         ; constant
    .divide:
        inc r12
        xor rdx, rdx
        div r11
        push rdx        ; save remainder
        test rax, rax      ; check if quotient zero
        jne .divide
    .print:
        dec r12
        pop rdi
        add rdi, "0"
        call print_char
        test r12, r12
        jns .print
    pop r12
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    .cycle:
        mov r10b, byte [rdi + rax]
        inc rax
        cmp r10b, byte [rsi + rax - 1]
        jne .no
        test r10b, r10b
        jne .cycle
    mov rax, 1
    ret
    .no:
        xor rax, rax
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    push ax 
    mov rdi, STDIN          ; stdin
    mov rsi, rsp            ; stack address
    mov rdx, 1              ; count
    syscall
    cmp rax, 1
    jne .err
    pop ax
    ret 
    .err:
        pop ax
        xor rax, rax
    ret

; Принимает: rdi - адрес начала буфера, rsi - размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax

read_word:
    push r12
    push r13
    push r14
    mov r12, rdi        ; buffer start
    mov r13, rsi        ; buffer size
    xor r14, r14        ; readed symbols count
    .cycle:
        xor rax, rax
        push ax
        mov rdi, STDIN          ; stdin
        mov rsi, rsp  ; address
        mov rdx, 1          ; count
        syscall
        pop di
        cmp rax, 1          ; check readed bytes count
        js .end
        test r14, r14      
        jne .write
        cmp rdi, ' '
        jbe .cycle       
        .write:
            cmp rdi, ' '   ; check spacing symbol
            jbe .end
            inc r14         ; readed bytes
            cmp r13, r14    ; check buffer length
            js .err
            mov byte [r12], dil
            inc r12
            cmp byte [r12 - 1], 0
        jne .cycle
    .end:
    mov byte [r12], 0   ; terminator
    sub r12, r14
    mov rax, r12        ; buffer start
    mov rdx, r14        ; readed bytes
    jmp .exit
    .err:
        xor rax, rax
    .exit:
        pop r14
        pop r13
        pop r12
    ret
 

; Принимает: rdi - указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rcx, rcx        ; symbols counter
    xor rax, rax        ; result number
    mov r10, 10         ; constant
    .cycle:
        movzx r11, byte [rdi]

        sub r11, "0"    ; remove ascii numbers offset
        js .end
        cmp r11, 10     ; check if it is number
        jns .end
        
        inc rdi
        inc rcx
        
        mul r10         ; rax = rax * 10
        add rax, r11
        
        jmp .cycle
    .end:
    mov rdx, rcx
    sub rdi, rdx
    ret




; Принимает rdi - указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    push r12
    xor r12, r12
    cmp byte[rdi], "-"
    jne .positive
    inc rdi
    inc r12
    .positive:
    call parse_uint
    test rdx, rdx
    je .exit
    test r12, r12
    je .exit
    neg rax
    dec rdi
    inc rdx
    .exit:
    pop r12
    ret
    
      

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax        ; string length
    .cycle:
        mov r10b, byte [rdi + rax]
        mov byte [rsi + rax], r10b
        inc rax
        cmp rdx, rax    ; check buffer overflow
        js .err
        test r10b, r10b
        jne .cycle
    ret
    .err:
        xor rax, rax
    ret

; Принимает rdi - указатель на буффер, rsi - требуемую длину строки (без учёта терминатора)
; Возвращает кол-во считанных символов
read_string:
    push rdi
    xor rax, rax
    mov rdx, rsi        ; count
    mov rsi, rdi        ; address
    mov rdi, STDIN      ; stdin
    syscall
    pop rdi
    test rax, rax
    js .err
    mov [rdi + rax + 1], 0
    ret
    .err:
        xor rax, rax
    ret
    
err_string:
    push rsi
    push r12
    mov r12, rdi             ; save address
    call string_length       ; rax now have string length
    mov     rdx, rax         ; string length
    mov     rax, SYS_WRITE   ; 'write' syscall number
    mov     rdi, STDERR      ; stderr
    mov     rsi, r12         ; string address        
    syscall
    mov rdi, r12
    pop r12
    pop rsi
    ret