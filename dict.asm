%include "lib.inc"
global find_word
global get_node_value

%define QWORD_SIZE 8
section .text
;Принимает два аргумента:

; rdi - указатель на нуль-терминированную строку.
; rsi - указатель на начало словаря.
; Если подходящее вхождение найдено, вернёт адрес начала вхождения в словарь (не значения), иначе вернёт 0
    find_word:
    push r12
    push r13
    mov r13, rdi
    xor rax, rax
    .cycle:
        mov r12, rsi
        lea rsi, [rsi + QWORD_SIZE]
        mov rdi, r13
        call string_equals
        test rax, rax
        jne .found
        cmp qword [r12], 0
        je .err
        mov rsi, [r12]
        jmp .cycle
    .found:
    mov rax, r12
    pop r13
    pop r12    
    ret
    .err:
        pop r13
        pop r12
        xor rax, rax
    ret
    
    
; rdi - адрес начала вхождения в словарь
; Возвращает адрес значения
get_node_value:
    push rdi
    lea rdi, [rdi + QWORD_SIZE]
    call string_length
    pop rdi
    lea rax, [rdi + rax + 1]
    ret