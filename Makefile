SHELL = zsh

init:	lib.o dict.o main.o
	@gcc -lc -no-pie -g main.o -o main lib.o dict.o

ERR_FILE = err.txt
key_not_found = Key wasn\'t found !
key1 = key pair1
key2 = key pair2
key3 = key pair3
key4 = $(shell printf "length of 255 !%.0s" {1..17})

value1 = value pair1
value2 = value pair2
value3 = value pair3
value4 = Value of key with 255 symbols

test:
	@date > $(ERR_FILE)
	@$(info Running tests...)
	@$(eval OUT := $(strip $(shell echo -n $(key1) | ./main 2>> $(ERR_FILE))))
	@if [ "$(OUT)" = "$(value1)" ]; then \
		echo "test1 passed"; \
	else \
		echo "test1 failed"; \
	fi;
	@$(eval OUT := $(strip $(shell echo -n $(key2) | ./main 2>> $(ERR_FILE))))
	@if [ "$(OUT)" = "$(value2)" ]; then \
		echo "test2 passed"; \
	else \
		echo "test2 failed"; \
	fi;
	@$(eval OUT := $(strip $(shell echo -n $(key3) | ./main 2>> $(ERR_FILE))))
	@if [ "$(OUT)" = "$(value3)" ]; then \
		echo "test3 passed"; \
	else \
		echo "test3 failed"; \
	fi;
	@$(eval OUT := $(strip $(shell echo -n $(key4) | ./main 2>> $(ERR_FILE))))
	@if [ "$(OUT)" = "$(value4)" ]; then \
		echo "test4 passed"; \
	else \
		echo "test4 failed"; \
	fi;
	@$(eval OUT := $(strip $(shell echo -n '1245dsfgdf3' | ./main 2>> $(ERR_FILE)))) 
	@if [ "$(OUT)" = "" ]; then \
		echo "test5 passed"; \
	else \
		echo "test5 failed"; \
	fi
	@$(eval OUT := $(strip $(shell echo -n '' | ./main 2>> $(ERR_FILE))))
	@if [ "$(OUT)" = "" ]; then \
		echo "test6 passed"; \
	else \
		echo "test6 failed"; \
	fi
	@$(eval OUT := $(strip $(shell echo -n $(value1) | ./main 2>> $(ERR_FILE))))
	@if [ "$(OUT)" = "" ]; then \
		echo "test7 passed"; \
	else \
		echo "test7 failed"; \
	fi
	@if [ $(shell cat $(ERR_FILE) | wc -l) != "1" ]; then \
		echo "errors:"; \
		cat $(ERR_FILE); \
	fi

%.o: %.asm
    @nasm -g -f elf64 -o %.o %.asm


clean:
	@rm -f ./*.o(N) main $(ERR_FILE)
