%include "lib.inc"
%include "dict.inc"

%include "words.inc"


section .bss
string: resb 256

section .rodata
read_err: db "Error reading stdin !", 0
find_err: db "Key wasn't found !", 0

section .text
global main
main:
    mov rdi, string
    mov rsi, 255
    call read_string
    test rax, rax
    js .read_err
    
    mov rdi, string
    mov rsi, length_test
    call find_word
    test rax, rax
    je .find_err
    
    mov rdi, rax
    call get_node_value
    mov rdi, rax
    call print_string
    
    xor rdi, rdi
    jmp exit
    .read_err:
        mov rdi, read_err
        call err_string
    jmp exit
    .find_err:
        mov rdi, find_err
        call err_string
    jmp exit
        